<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Param
 * 
 * @property int $id
 * @property string $key
 * @property array $value
 *
 * @package App\Models
 */
class Param extends Model
{
	protected $table = 'params';
	public $timestamps = false;

	protected $casts = [
		'value' => 'json'
	];

	protected $fillable = [
		'key',
		'value'
	];

  public function value(){
    return json_encode($this->value, JSON_UNESCAPED_UNICODE );
  }
}
