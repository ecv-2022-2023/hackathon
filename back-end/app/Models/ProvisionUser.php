<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProvisionUser
 * 
 * @property int $user_id
 * @property int $provision_id
 * 
 * @property Provision $provision
 * @property User $user
 *
 * @package App\Models
 */
class ProvisionUser extends Model
{
	protected $table = 'provision_user';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'provision_id' => 'int'
	];

	public function provision()
	{
		return $this->belongsTo(Provision::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
