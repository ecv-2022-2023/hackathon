<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Provision
 * 
 * @property int $id
 * @property string $name
 * @property string $ref
 * @property string $image
 * @property string $category
 * @property string|null $description
 * @property int $rank
 * 
 * @property Collection|User[] $users
 *
 * @package App\Models
 */
class Provision extends Model
{
	protected $table = 'provisions';
	public $timestamps = false;

	protected $casts = [
		'rank' => 'int'
	];

	protected $fillable = [
		'name',
		'ref',
		'image',
		'category',
		'description',
		'rank'
	];

	public function users()
	{
		return $this->belongsToMany(User::class);
	}
}
