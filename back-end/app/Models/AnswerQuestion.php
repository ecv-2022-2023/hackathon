<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AnswerQuestion
 * 
 * @property int $answer_id
 * @property int $question_id
 * @property string|null $value
 * 
 * @property Answer $answer
 * @property Question $question
 *
 * @package App\Models
 */
class AnswerQuestion extends Model
{
	protected $table = 'answer_question';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'answer_id' => 'int',
		'question_id' => 'int'
	];

	protected $fillable = [
		'value'
	];

	public function answer()
	{
		return $this->belongsTo(Answer::class);
	}

	public function question()
	{
		return $this->belongsTo(Question::class);
	}
}
