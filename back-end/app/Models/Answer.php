<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Answer
 * 
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $type
 * @property int $user_id
 * 
 * @property User $user
 * @property Collection|Question[] $questions
 *
 * @package App\Models
 */
class Answer extends Model
{
	protected $table = 'answers';

	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'type',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function questions()
	{
		return $this->belongsToMany(Question::class)
					->withPivot('value');
	}
}
