<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Question
 * 
 * @property int $id
 * @property string $label
 * @property string $form
 * @property string $type
 * @property string $category
 * @property string|null $description
 * @property string|null $helper
 * @property bool $active
 * 
 * @property Collection|Answer[] $answers
 *
 * @package App\Models
 */
class Question extends Model
{
	protected $table = 'questions';
	public $timestamps = false;

	protected $casts = [
		'active' => 'bool'
	];

	protected $fillable = [
		'label',
		'form',
		'type',
		'category',
		'description',
		'helper',
		'active'
	];

	public function answers()
	{
		return $this->belongsToMany(Answer::class)
					->withPivot('value');
	}
}
