<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSensibility
 * 
 * @property int $id
 * @property int $light
 * @property int $noise
 * @property int $smell
 * @property int $atmosphere
 * @property int $presence
 * @property int $user_id
 * 
 * @property User $user
 *
 * @package App\Models
 */
class UserSensibility extends Model
{
	protected $table = 'user_sensibilities';
	public $timestamps = false;

	protected $casts = [
		'light' => 'int',
		'noise' => 'int',
		'smell' => 'int',
		'atmosphere' => 'int',
		'presence' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'light',
		'noise',
		'smell',
		'atmosphere',
		'presence',
		'user_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}
