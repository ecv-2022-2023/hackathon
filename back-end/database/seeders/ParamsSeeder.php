<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ParamsSeeder extends Seeder
{
    static $params = [
      "ONBOARDING_REMOTE" => ["Pas de télétravail" => 0, "1 jour par semaine" => 1, "2 jours par semaine" => 2, "100 télétravail" => 5],
      "ONBOARDING_MATERIAL" => [
        [
          "title" => "Ordinateur · Apple MacBook Pro 13” M2",
          "questions" => [
            [
              "libelle" => "Tu peux choisir la couleur",
              "code" => "COULEUR_ORDINATEUR",
              "choices" => ["Gris sidéral", "Argent"]
            ],
          ]
        ],
        [
          "title" => "Bureau et installation",
          "questions" => [
            [
              "libelle" => "Tu es plutôt de la team",
              "code" => "POSITION_TRAVAIL",
              "choices" => ["Je travaille assis", "Je travaille debout", "Les deux"]
            ],
          ]
        ],
        [
          "title" => "Côté souris",
          "questions" => [
            [
              "libelle" => "Tu travailles avec",
              "code" => "MAIN_UTILISE",
              "choices" => ["La main gauche", "La main droite", "Trackpad team"]
            ],
            [
              "libelle" => "Quelle souris te convient le mieux ?",
              "code" => "MODELE_SOURIS",
              "choices" => ["Logitech Souris Ergonomique Verticale Lift", "Logitech Souris MX Ergo Trackball"]
            ],
            [
              "libelle" => "Autre modèle, à toi de le préciser ?",
              "code" => "MODELE_SOURIS",
              "type" => "OPEN"
            ],
          ]
        ],
      ]
    ];
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
      foreach (ParamsSeeder::$params as $key => $value) {
        DB::table('params')->insert([
            'key' => $key,
            'value' => json_encode($value),
        ]);
      }
    }
}
