<?php
 
namespace Database\Seeders;
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
 
class ProvisionFaker extends Seeder
{
    /**
     * Run the database seeders.
     */
    public function run(): void
    {
      for ($i=0; $i < 10; $i++) {
        $faker = \Faker\Factory::create(); 
        DB::table('provisions')->insert([
            'name' => $faker->words(3, true),
            'image' => $faker->imageUrl(250, 100),
            'ref' => $faker->ean13(),
            'category' => $faker->randomElement(['ORDINATEUR', 'MOBILIER', 'CONFORT', 'GADGET']),
            'description' => $faker->words(25, true),
            'rank' => 1
        ]);
      }
    }
}