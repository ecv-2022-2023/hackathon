<?php
 
namespace Database\Seeders;
 
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
 
class UserFaker extends Seeder
{
    /**
     * Run the database seeders.
     */
    public function run(): void
    {
      for ($i=0; $i < 10; $i++) {
        $faker = \Faker\Factory::create(); 
        DB::table('users')->insert([
            'name' => $faker->name(),
            'email' => $faker->safeEmail(),
            'password' => bcrypt('test'),
        ]);
      }
    }
}