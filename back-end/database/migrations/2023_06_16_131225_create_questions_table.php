<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->string('form', 50);
            $table->string('type', 25);
            $table->string('category', 25);
            $table->string('description')->nullable();
            $table->string('helper')->nullable();
            $table->boolean('active')->default(1);
            $table->boolean('required')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('questions');
    }
};
