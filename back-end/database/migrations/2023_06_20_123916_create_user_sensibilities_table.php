<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('user_sensibilities', function (Blueprint $table) {
            $table->id();
            $table->integer('light');
            $table->integer('noise');
            $table->integer('smell');
            $table->integer('atmosphere');
            $table->integer('presence');
            $table->foreignId('user_id')->references('id')->on('users');
            $table->unique('user_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('user_sensibilities');
    }
};
