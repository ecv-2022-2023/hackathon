import { createBrowserRouter } from 'react-router-dom'
import NotFoundPage from './pages/NotFoundPage'
import App from './App'
import Login from './pages/Login'
import Logout from './pages/Logout'
import FormNeeds from './pages/Forms/Needs/Needs'
import Ressources from './pages/Ressources'
import Home from './pages/Home/Home'

const router = createBrowserRouter([
  {
    path: '/',
    element: <Home />
  },
  {
    path: '/login',
    element: <Login />
  },
  {
    path: '/logout',
    element: <Logout />
  },
  {
    path: '/form-needs',
    element: <FormNeeds />
  },
  {
    path: '/ressources',
    element: <Ressources />
  },
  {
    path: '*',
    element: <NotFoundPage />
  }
])

export default router
