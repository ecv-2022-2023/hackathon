import { getBearerToken } from '../../services/AuthService'
import React from 'react'
import { Link } from 'react-router-dom'

function Navbar () {
  return (
    <ul>
      <li>
        <Link to='/'>Home</Link>
      </li>
      <li>
        {!getBearerToken() ? (
          <Link to='/login'>Login</Link>
        ) : (
          <Link to='/logout'>Logout</Link>
        )}
      </li>
      <li>
        <Link to='/form-needs'>Form : Needs</Link>
      </li>
      <li>
        <Link to='/ressources'>Ressources</Link>
      </li>
    </ul>
  )
}

export default Navbar
