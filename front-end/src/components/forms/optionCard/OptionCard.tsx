type OptionCardProps = {
  title: string,
  description?: string,
  image?: string,
  active: boolean,
  callback: Function
}

const OptionCard = (props: OptionCardProps) => {
  const { title, description, image, active, callback } = props

  return (
    <li onClick={() => callback()} className={`option-card ${active ? 'active' : ''}`}>
      {image ? <img className="option-card__image" src={image} /> : null}
      <p className="option-card__title" >{title}</p>
      {description ? <p className="option-card__description" >{description}</p> : null}
    </li>
  )
}

export default OptionCard