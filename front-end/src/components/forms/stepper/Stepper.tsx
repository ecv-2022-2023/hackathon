import { StepperState } from '../../../types/enums'
import React from 'react'

function Stepper ({
  step,
  category,
  state,
  goTo,
  isLast
}: {
  step: number
  category: string
  state: StepperState
  goTo: (num: number) => void
  isLast: boolean
}) {
  let stateClass = ''

  if (state === StepperState.SUCCESS) {
    stateClass = '--success'
  } else if (state === StepperState.ERROR) {
    stateClass = '--error'
  } else if (state === StepperState.CURRENT) {
    stateClass = '--current'
  } else if (state === StepperState.EMPTY) {
    stateClass = '--empty'
  }

  return (
    <li
      onClick={() => {
        goTo(step)
      }}
      className={`stepper stepper${stateClass}`}
    >
      <div className='stepper__header'>
        <figure className={`stepper__shape stepper__shape${stateClass}`}>
          <img src={`/assets/icons/steppers/step${step}.svg`} alt='' />
        </figure>
        {isLast || <span className={`stepper__separator stepper__separator--${step}`}></span>}
      </div>
      <div className='stepper__content'>
        <p className='stepper__step'>étape {step}</p>
        <p className='stepper__category'>{category}</p>
      </div>
    </li>
  )
}

export default Stepper
