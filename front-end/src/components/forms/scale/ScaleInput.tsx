
type OptionScaleInput = {
  leftText?: string,
  rightText?: string,
  defaultValue?: string,
  callback: Function
}

const ScaleInput = (props: OptionScaleInput) => {
  const {leftText, rightText, defaultValue, callback} = props

  return (
    <div>
      { leftText ? <p>{leftText}</p> : null}
      <input type="range" min="0" max="100" defaultValue={defaultValue ?? 50} step="5" onChange={(e: any) => callback(e.target.value)}/>
      { rightText ? <p>{rightText}</p> : null}
    </div>
  )
}

export default ScaleInput