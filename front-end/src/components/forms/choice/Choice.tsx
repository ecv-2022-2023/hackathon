type OptionChoice = {
  title: string,
  value: string,
  active: boolean,
  callback: Function
}

const Choice = (props: OptionChoice) => {
  const {title, value, active, callback} = props
  return (
    <div onClick={() => callback(value)} className={`choice ${active ? 'active' : ''}`}>
      <p className="choice__content" >{title}</p>
    </div>
  )
}

export default Choice