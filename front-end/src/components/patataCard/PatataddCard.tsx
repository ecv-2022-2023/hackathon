
import { ReactComponent as AddCircleIcon }  from "../../assets/icons/add-circle.svg";

const PatataddCard = (props : any) => {
  const { children, patata, color } = props
  return (
    <div className="card card--patatadd" style={{"--color": color} as React.CSSProperties }>
      { children}
      <AddCircleIcon /> 
      <img src={`/assets/patatas/patata-${patata}-angle.svg`} className="card--patatadd__image" />
    </div>
  )
}

export default PatataddCard