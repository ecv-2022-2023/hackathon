import { ApolloClient, ApolloLink, HttpLink, InMemoryCache } from '@apollo/client';
import { setContext } from 'apollo-link-context';
import { createUploadLink } from 'apollo-upload-client';
import { getBearerToken } from './services/AuthService'


const authLink = setContext((_, { headers }) => {
  // Ajoutez vos en-têtes personnalisés ici
  let customHeaders = {

  }
  if(getBearerToken()){
    customHeaders = {
      // Exemple : ajouter un en-tête d'autorisation
      Authorization: `Bearer ${getBearerToken()}` ,
    };
  }

  return {
    headers: {
      ...headers,
      ...customHeaders,
    }
  };
}) as any;

const uploadLink = createUploadLink({
  uri: import.meta.env.VITE_API_ENDPOINT,
})


const link = ApolloLink.from([authLink, uploadLink]);

const blog = new HttpLink({
  uri: import.meta.env.VITE_BLOG_ENDPOINT,
})

const client = new ApolloClient({
  link: ApolloLink.split(
    operation => operation.getContext().clientName === 'blog',
    blog,
    link
  ) ,
  cache: new InMemoryCache(),
  defaultOptions: {
    query: {
      fetchPolicy: 'no-cache',
      headers: {
        'Content-Type': 'application/json',
      },
      fetchOptions: {
        mode: 'no-cors', // no-cors, *cors, same-origin
      }
    } as any,
    mutate: {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    } as any,
  },
} as any);

export default client;