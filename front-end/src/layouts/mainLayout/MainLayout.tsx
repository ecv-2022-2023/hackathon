
import { ReactComponent as Home }  from "../../assets/icons/home.svg";
import { ReactComponent as Bell }  from "../../assets/icons/bell.svg";
import { ReactComponent as Search }  from "../../assets/icons/search.svg";
import { ReactComponent as ChevronBottom }  from "../../assets/icons/chevron-bottom.svg";
import { ReactComponent as Logo }  from "../../assets/identity/logo.svg";

const MainLayout = (props: any) => {
  const { children, className } = props
  return (
    <>
      <div className={`main-layout ${className} `}>
        <div className="main-layout__header">
          <Logo className="logo" width="76" />
          <div className="actions">
            <div className="input-container">
              <Search className="input-container__icon" />
              <input className="input-container__input" type="search" placeholder="Cherchez un collaborateur, une tâche..." />
            </div>
            <Bell />
            <div className="profile">
              <img width="34" height="34" src="/images/barnabe.jpg" className="profile__img" />
              <p className="profile__name">Stéphane Gentil</p>
              <p className="profile__job">Directeur RH</p>
              <ChevronBottom className="profile__icon" />
            </div>
          </div>
        </div>
        <div className="main-layout__sidebar">
          <ul>
            <li className="active">
              <Home />
              <p>Accueil</p>
            </li>
            <li>
              <Home />
              <p>Agenda</p>
            </li>
            <li>
              <Home />
              <p>Ressources</p>
            </li>
            <li>
              <Home />
              <p>Suivi collaborateurs</p>
            </li>
            <li>
              <Home />
              <p>Espace entreprise</p>
            </li>
          </ul>
        </div>
        <main className="main-layout__main">
          { children }
        </main>
      </div>
    </>
  )
}

export default MainLayout