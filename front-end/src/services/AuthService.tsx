const setBearerToken = (token: string): void => {
  localStorage.setItem('huna_bearer_token', token)
}

const getBearerToken = (): string|boolean => {
  return localStorage.getItem('huna_bearer_token') ?? false
}

const removeBearerToken = (): void => {
  localStorage.removeItem('huna_bearer_token')
}

export { getBearerToken, setBearerToken, removeBearerToken }
