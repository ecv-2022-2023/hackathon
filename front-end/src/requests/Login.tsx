import gql from 'graphql-tag'

const LOGIN = gql`
  mutation login($input: LoginInput!) {
    login(input: $input) {
      access_token
    }
  }
`

export default LOGIN
