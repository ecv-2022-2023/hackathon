/* STEPPER STATE */

export enum StepperState {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  CURRENT = 'CURRENT',
  EMPTY = 'EMPTY'
}