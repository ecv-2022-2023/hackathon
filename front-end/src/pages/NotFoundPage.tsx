import Navbar from '../components/navbar/Navbar'
import React from 'react'

function NotFoundPage () {
  return (
    <div className='notfound'>
      <Navbar />
      <h1>Erreur 404 - Page non trouvée</h1>
      <p>Désolé, la page que vous recherchez n'existe pas.</p>
      <a href='/' className='button'>
        Retourner à la page d'accueil
      </a>
    </div>
  )
}

export default NotFoundPage
