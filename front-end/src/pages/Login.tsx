import { useMutation } from '@apollo/client'
import Navbar from '../components/navbar/Navbar'
import React from 'react'
import { useForm, SubmitHandler } from 'react-hook-form'
import LOGIN from '../requests/Login'
import { setBearerToken } from '../services/AuthService'

function Login () {
  const {
    register,
    handleSubmit,
    watch,
    formState: { errors }
  } = useForm<LoginForm>()

  const [logUser, { data: logData, loading: logLoading, error: logError }] =
    useMutation(LOGIN)

  const onSubmit: SubmitHandler<LoginForm> = async data => {
    const userExist = await logUser({
      variables: {
        input: {
          username: data.email,
          password: data.password
        }
      }
    })
    // Init bearer token
    setBearerToken(userExist.data.login.access_token)
  }

  return (
    <div>
      <Navbar />
      {/* "handleSubmit" will success your inputs before invoking "onSubmit" */}
      <form onSubmit={handleSubmit(onSubmit)}>
        {/* register your input into the hook by invoking the "register" function */}
        <label htmlFor='email'>Email</label>
        <input id='email' {...register('email', { required: true })} />

        {/* include validation with required or other standard HTML validation rules */}
        <label htmlFor='password'>Mot de passe</label>
        <input
          id='password'
          type='password'
          {...register('password', { required: true })}
        />
        {/* errors will return when field validation fails  */}
        {errors.email && <span>Email is required</span>}
        {errors.password && <span>Password is required</span>}
        {logError && <p>La combinaison email / mot de passe est incorrecte.</p>}

        <input type='submit' />
      </form>
    </div>
  )
}

export default Login
