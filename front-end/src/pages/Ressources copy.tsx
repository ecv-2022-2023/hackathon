import MainLayout from "../layouts/mainLayout/MainLayout"
import { useQuery } from "@apollo/client"
import gql from 'graphql-tag'



const GET_ARTICLES = gql`
query getArticles{
  articles{
    data {
      attributes {
        titre 
        slug 
        temps_de_lecture
        categories{
          data { 
            attributes {
              nom
            }
          }
        }
      }
    }
  }
}

`

const Ressources = () => {
  
  const {data} = useQuery(GET_ARTICLES, {context: {clientName: 'blog'}})
  return (
    <>
      <MainLayout>
        <p>Ressources</p>
        <p>{JSON.stringify(data)}</p>
      </MainLayout>
    </>
  )
}

export default Ressources