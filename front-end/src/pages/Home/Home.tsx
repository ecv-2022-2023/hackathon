import MainLayout from '../../layouts/mainLayout/MainLayout'
import React from 'react'
import PatataddCard from '../../components/patataCard/PatataddCard'
function Home () {
  return (
    <MainLayout className="homepage">
      <section>
      <h2 className="title--1">Bonjour Stéphane, <br/>besoin de quelque chose ?</h2>
        <div className="need-entrances">
        <PatataddCard patata="house" color="#F8D576">
          <h3>Vie en <br/>entreprise</h3>
        </PatataddCard>
        <PatataddCard patata="provisions" color="#B0D1EB">
          <h3>Matériel</h3>
        </PatataddCard>
        <PatataddCard patata="workspace" color="#F8C7AB">
          <h3>Espace <br/>de travail</h3>
        </PatataddCard>
        <PatataddCard patata="health" color="#FCBFC4">
          <h3>Santé</h3>
        </PatataddCard>
        </div>
      </section>
    </MainLayout>
  )
}

export default Home
