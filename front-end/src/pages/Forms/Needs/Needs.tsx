import Stepper from '../../../components/forms/stepper/Stepper'
import { StepperState } from '../../../types/enums'
import React, { useEffect, useState } from 'react'
import { useMutation, useQuery } from '@apollo/client'
import gql from 'graphql-tag'
import ScaleInput from '../../../components/forms/scale/ScaleInput'
import Choice from '../../../components/forms/choice/Choice'
import NeedsHeader from './NeedsHeader'
import NeedsFooter from './NeedsFooter'

const GET_FORM_NEEDS_DATA = gql`
  query {
    remote: param(key: "ONBOARDING_REMOTE") {
      value
    }
    material: param(key: "ONBOARDING_MATERIAL") {
      value
    }
  }
`

const SUBMIT_NEED = gql`
  mutation updateUserFromNeed(
    $sensibilityInput: CreateUserSensibilityInput!
    $userInput: UpdateUserInput!
  ) {
    updateUser(input: $userInput) {
      id
    }
    createUserSensibility(input: $sensibilityInput) {
      id
    }
  }
`

function Needs () {
  const [step, setStep] = useState(1)
  const { data } = useQuery(GET_FORM_NEEDS_DATA, {})
  const [sendNeeds, {}] = useMutation(SUBMIT_NEED)

  const [provisionsSelected, setProvisionsSelected] = useState<any>({})

  const [allergy, setAllergy] = useState(null)
  const [health, setHealth] = useState(null)

  const [sensibilities, setSensibilities] = useState<any>({
    light: null,
    ambiance: null,
    sound: null,
    presence: null,
    smell: null
  })

  const setSensibility = (sensibility: string, value: number) => {
    sensibilities[sensibility] = value
    setSensibilities({ ...sensibilities })
  }

  const setProvision = (code: string, value: string) => {
    provisionsSelected[code] = value
    setProvisionsSelected({ ...provisionsSelected })
  }

  const [presence, setPresence] = useState<string | null>(null)
  useEffect(() => {
    if (data?.material?.value) {
      const material = JSON.parse(data.material.value)
      material.map((category: any) => {
        category.questions.map((question: any) => {
          provisionsSelected[question.code] = null
        })
      })

      setProvisionsSelected({ ...provisionsSelected })
    }
  }, [data])

  const goTo = (num: number): void => {
    setStep(num)
  }

  const submit = async () => {
    const variables = {
      userInput: {
        id: 1,
        favorite_remote: presence?.toString(),
        favorite_provisions: JSON.stringify(provisionsSelected),
        health: health,
        allergies: allergy
      },
      sensibilityInput: {
        user_id: 1,
        light: sensibilities.light.toString(),
        smell: sensibilities.smell.toString(),
        noise: sensibilities.sound.toString(),
        presence: sensibilities.presence.toString(),
        atmosphere: sensibilities.ambiance.toString()
      }
    }

    const result = sendNeeds({ variables: variables })
    alert(JSON.stringify(result))
  }
  const getBottomNav = () => {
    if (step == 1) {
      return (
        <button onClick={() => goTo(2)} className='button button--full'>
          Question suivante
          <img
            src='/assets/icons/buttons/arrow_right.svg'
            alt='icon arrow right'
          />
        </button>
      )
    } else if (step == steps.length) {
      return (
        <>
          <button onClick={() => goTo(step - 1)} className='button'>
            Réponse précédente
          </button>
          <button onClick={submit} className='button'>
            J'envoie ma réponse
          </button>
        </>
      )
    } else {
      return (
        <>
          <button onClick={() => goTo(step - 1)} className='button'>
            Réponse précédente
          </button>
          <button onClick={() => goTo(step + 1)} className='button'>
            Question suivante
            <img
              src='/assets/icons/buttons/arrow_right.svg'
              alt='icon arrow right'
            />
          </button>
        </>
      )
    }
  }

  const steps = [
    {
      title: 'Quel mode de travail vous convient le mieux ?',
      category: 'Vie en entreprise',
      desc: 'Afin que nous puissions arranger nos emplois du temps pour que tu sois le plus à l’aise possible.',
      state: step === 1 ? StepperState.CURRENT : StepperState.EMPTY,
      render: () => {
        if (!data) return <></>
        const possibilities = JSON.parse(data.remote.value)
        return (
          <div className='choices'>
            {Object.keys(possibilities).map((key: any) => {
              return (
                <Choice
                  title={key}
                  value={possibilities[key]}
                  callback={(value: string) => setPresence(value)}
                  active={presence == possibilities[key]}
                />
              )
            })}
          </div>
        )
      }
    },
    {
      title: 'Tu es libre de choisir ton matériel ici bas.',
      category: 'Matériel souhaité',
      desc: 'Si tu as quelque chose d’autre en tête, n’hésite pas à nous le préciser.',
      state: step === 2 ? StepperState.CURRENT : StepperState.EMPTY,
      render: () => {
        return (
          <>
            <p>{JSON.stringify(provisionsSelected)}</p>
            {(data?.material ? JSON.parse(data.material.value) : []).map(
              (category: any) => {
                return (
                  <>
                    <h2>{category.title}*</h2>
                    {category.questions.map((question: any) => {
                      return (
                        <>
                          <p>{question.libelle}</p>
                          {question.choices ? (
                            <div className='choices'>
                              {question.choices.map((choice: string) => (
                                <>
                                  <Choice
                                    title={choice}
                                    value={choice}
                                    active={
                                      choice ==
                                      provisionsSelected[question.code]
                                    }
                                    callback={(value: string) =>
                                      setProvision(question.code, value)
                                    }
                                  />
                                </>
                              ))}
                            </div>
                          ) : (
                            <textarea className='choice__textarea'></textarea>
                          )}
                        </>
                      )
                    })}
                  </>
                )
              }
            )}
          </>
        )
      }
    },
    {
      title: 'Quel est ton cadre de travail idéal ?',
      category: 'Cadre de travail',
      desc: 'Nous allons t’aider à créer ton cadre de travail idéal, pour cela, nous avons besoin de tes retours.',
      state: step === 3 ? StepperState.CURRENT : StepperState.EMPTY,
      render: () => {
        return (
          <>
            <p>{JSON.stringify(sensibilities)}</p>
            <label>
              <ScaleInput
                leftText='Je ne suis pas du tout sensible à la lumière'
                rightText='Je suis très sensible à la lumière'
                callback={(value: any) => setSensibility('light', value)}
              />
              <ScaleInput
                leftText="J'ai eu le covid"
                rightText='Je ne supporte pas les odeurs (nourriture, parfum...)'
                callback={(value: any) => setSensibility('smell', value)}
              />
              <ScaleInput
                leftText="J'ai besoin d'avoir mon bureau seul(e)"
                rightText="J'ai besoin d'être entouré(e)"
                callback={(value: any) => setSensibility('presence', value)}
              />
              <ScaleInput
                leftText='Pour travailler, il me faut une pièce neutre : murs blancs et matériel pro seulement'
                rightText='Pour travailler, il me faut une pièce stimulante : couleurs, décoration...'
                callback={(value: any) => setSensibility('ambiance', value)}
              />
              <ScaleInput
                leftText="Le monde peut s'écrouler que j'entendrais rien"
                rightText='Je suis très sensible aux bruits'
                callback={(value: any) => setSensibility('sound', value)}
              />
            </label>
          </>
        )
      }
    },
    {
      title: 'Souhaitez-vous préciser quelque chose sur votre santé ?',
      category: 'Santé',
      desc: 'Cela peut concerner des habitudes alimentaires, des soins, des allergies et cela nous permettra d’anticiper tes besoins.',
      state: step === 4 ? StepperState.CURRENT : StepperState.EMPTY,
      render: () => {
        return (
          <>
            <label>Vos allergies</label>
            <textarea
              onInput={(e: any) => setAllergy(e.target.value)}
            ></textarea>

            <label>Vos besoins de santé</label>
            <textarea
              onInput={(e: any) => setHealth(e.target.value)}
            ></textarea>
          </>
        )
      }
    }
  ]

  return (
    <div className='needs'>
      <nav className='needs__nav'>
        <ul className='steppers'>
          {steps.map((step: any, i: number, row: any) => {
            return (
              <Stepper
                key={i}
                step={i + 1}
                category={step.category}
                state={step.state}
                goTo={goTo}
                isLast={i + 1 === row.length ? true : false}
              />
            )
          })}
        </ul>
      </nav>
      <div className='needs__inner'>
        <NeedsHeader
          step={step}
          subtitle={(steps[step - 1] as any).title}
          desc={(steps[step - 1] as any).desc}
        />
        {(steps[step - 1] as any).render()}
        <div className='needs__footer'>{getBottomNav()}</div>
      </div>
      <NeedsFooter />
    </div>
  )
}

export default Needs
