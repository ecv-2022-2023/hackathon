import React from 'react'

function NeedsHeader ({
  step,
  subtitle,
  desc
}: {
  step: number
  subtitle: string
  desc: string
}) {
  return (
    <header className='needs__header'>
      <h1 className='needs__header-title'>Bienvenue à bord Barnabé</h1>
      <div className='needs__header-inner'>
        <figure className='needs__header-figure'>
          <img src={`/assets/icons/steppers/step${step}.svg`} alt='' />
        </figure>
        <h2 className='needs__header-subtitle'>{subtitle}</h2>
        <p className='needs__header-desc'>{desc}</p>
      </div>
    </header>
  )
}

export default NeedsHeader
