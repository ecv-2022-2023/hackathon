import React from 'react'

function NeedsFooter () {
  return (
    <footer className='needs__footer'>
      <p>©Tous droits réservés</p>
    </footer>
  )
}

export default NeedsFooter
