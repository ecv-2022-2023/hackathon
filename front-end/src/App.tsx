import React from 'react'
import Navbar from './components/navbar/Navbar'

function App () {
  return (
    <>
      <Navbar />
      <p>Api endpoint : {import.meta.env.VITE_API_ENDPOINT}</p>
      <h1>🍞 Bienvenue chez Huna 🥝</h1>
      <div className='card'>
        <p>
          Edit <code>src/App.tsx</code> and save to test HMR
        </p>
      </div>
      <p className='read-the-docs'>
        Click on the Vite and React logos to learn more
      </p>
    </>
  )
}

export default App
